var mergeModule = require('webpack-merge');
var merge = mergeModule.merge;
module.exports = {
  productionSourceMap: process.env.NODE_ENV !== "production",
  configureWebpack: config => {
    merge(config,
      {
        VUE_APP_BACKEND_URL: process.env.VUE_APP_BACKEND_URL,
        VUE_APP_FIREBASE_CREDENTIALS_JSON: process.env.VUE_APP_FIREBASE_CREDENTIALS_JSON
      }
    );
  }
};

