import { createRouter, createWebHistory } from "@ionic/vue-router";
import Home from "../views/tabs/Home.vue";
import Goals from "../views/tabs/Goals.vue";
import OwnProfile from "../views/tabs/OwnProfile.vue";
import Tabs from "../views/tabs/Tabs.vue";
import Login from "../views/Login.vue";
import Comments from "../views/Comments.vue";
import Goal from "../views/Goal.vue";
import Followers from "../views/Followers.vue";
import Following from "../views/Following.vue";
import Profile from "../views/Profile.vue";
import SearchUsers from "../views/SearchUsers.vue";
import Supporters from "../views/Supporters.vue";
import AboutUs from "../views/AboutUs.vue";
import CreationGoalTitle from "../views/goal-creation/Title.vue";
import CreationGoalDescription from "../views/goal-creation/Description.vue";
import CreationGoalSteps from "../views/goal-creation/Steps.vue";
import CreationGoalStatus from "../views/goal-creation/Status.vue";
import FeedbackAndSupport from "../views/FeedbackAndSupport.vue";
import Settings from "../views/Settings.vue";
import PostCreation from "../views/PostCreation.vue";
import store from "../store";
import firebaseAnalyticsService from "../services/firebase-analytics";

const tabRoutes = {
  path: "/tabs/",
  component: Tabs,
  children: [
    {
      path: "home",
      name: "Home",
      component: Home,
    },
    {
      path: "goals",
      name: "Goals",
      component: Goals,
    },
    {
      path: "me",
      name: "OwnProfile",
      component: OwnProfile,
    },
  ],
};

const routes = [
  tabRoutes,
  {
    path: "/",
    redirect: { name: "Home" },
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/posts/:id/comments",
    name: "Comments",
    component: Comments,
  },
  {
    path: "/profile/:userId",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/users/search",
    name: "SearchUsers",
    component: SearchUsers,
  },
  {
    path: "/feedback-and-support",
    name: "FeedbackAndSupport",
    component: FeedbackAndSupport,
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
  },
  {
    path: "/goals/:goalId",
    name: "Goal",
    component: Goal,
  },
  {
    path: "/goals/create/title",
    name: "CreationGoalTitle",
    component: CreationGoalTitle,
  },
  {
    path: "/goals/create/description",
    name: "CreationGoalDescription",
    component: CreationGoalDescription,
  },
  {
    path: "/goals/create/steps",
    name: "CreationGoalSteps",
    component: CreationGoalSteps,
  },
  {
    path: "/goals/create/status",
    name: "CreationGoalStatus",
    component: CreationGoalStatus,
  },
  {
    path: "/users/:userId/followers",
    name: "Followers",
    component: Followers
  },
  {
    path: "/users/:userId/following",
    name: "Following",
    component: Following
  },
  {
    path: "/posts/create",
    name: "PostCreation",
    component: PostCreation,
  },
  {
    path: "/me/:postId/supporters",
    name: "Supporters",
    component: Supporters
  },
  {
    path: "/about-us",
    name: "AboutUs",
    component: AboutUs,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

//Redirects user to login page when he's not connected
router.beforeEach((to, from, next) => {
  firebaseAnalyticsService.logEvent(
    "page_view",
    {
      page_referrer: from.fullPath,
      page_location: to.fullPath,
    }
  );
  const isConnected = !!store.state.user;
  const destinationIsLoginPage = to.name === "Login";
  if (!isConnected && !destinationIsLoginPage) {
    next({ name: "Login" });
  } else if (isConnected && destinationIsLoginPage) {
    next({ name: "Home" });
  } else {
    next();
  }
});

export default router;
