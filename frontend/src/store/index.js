import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";
//TODO: faire recherches sur l'encodage et compression optimale
const ls = new SecureLS({ isCompression: false });

export default createStore({
  state: {
    user: null,
    idToken: null,
    refreshToken: null,
    createdGoal: {
      title: null,
      description: null,
      steps: [],
      status: null,
    },
    isInDarkMode: window.matchMedia("(prefers-color-scheme: dark)").matches,
    areAnalyticsEnabled: true,
    appLang: "en",
  },
  actions: {
    setUser({ commit }, user) {
      commit("setUser", user);
    },
    setIdToken({ commit }, idToken) {
      commit("setIdToken", idToken);
    },
    setRefreshToken({ commit }, refreshToken) {
      commit("setRefreshToken", refreshToken);
    },
    setDarkMode({ commit }, isInDarkMode) {
      commit("setDarkMode", isInDarkMode);
    },
    deleteCreatedGoal({ commit }) {
      commit("deleteCreatedGoal");
    },
    setAnalyticsMode({ commit }, areAnalyticsEnabled) {
      commit("setAnalyticsMode", areAnalyticsEnabled);
    },
    setAppLang({ commit }, appLang) {
      commit("setAppLang", appLang);
    },
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setIdToken(state, idToken) {
      state.idToken = idToken;
    },
    deleteCreatedGoal(state) {
      state.createdGoal = {
        title: null,
        description: null,
        steps: [],
        status: null,
      };
    },
    setRefreshToken(state, refreshToken) {
      state.refreshToken = refreshToken;
    },
    setDarkMode(state, isInDarkMode) {
      state.isInDarkMode = isInDarkMode;
    },
    setAnalyticsMode(state, areAnalyticsEnabled) {
      state.areAnalyticsEnabled = areAnalyticsEnabled;
    },
    setAppLang(state, appLang) {
      state.appLang = appLang;
    },
  },
  modules: {},
  plugins: [
    createPersistedState({
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key),
      },
    }),
  ],
});
