import firebase from "firebase/app";
import store from "../store";
import "firebase/auth";
import axios from "axios";
import firebaseAnalyticsService from "./firebase-analytics";

export default {
  firebaseCredentials: JSON.parse(process.env.VUE_APP_FIREBASE_CREDENTIALS_JSON),
  async signInWithGoogle() {
    return firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
  },
  setUp() {
    firebase.initializeApp(this.firebaseCredentials);
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        firebaseAnalyticsService.setUserId(user.uid);
        store.dispatch("setUser", user);
        store.dispatch("setIdToken", user.ya);
        store.dispatch("setRefreshToken", user.b.a);
      } else {
        store.dispatch("setUser", null);
      }
    });
  },
  async signOut() {
    await firebase.auth().signOut();
    window.location.reload();
  },
  async refreshIdToken() {
    const baseURL = "https://securetoken.googleapis.com/v1/token";
    const idTokenResponse = await axios.post(
      baseURL + "?key=" + this.firebaseCredentials.apiKey,
      {
        grant_type: "refresh_token",
        refresh_token: store.state.refreshToken,
      },
      {
        headers: { "content-type": "application/json" },
      }
    );
    store.dispatch("setIdToken", idTokenResponse.data.id_token);
  },
};
