import getBackendApi from "./backend-api";

const BASE_ROUTE = "steps";

export default {
  async create(step) {
    return (await getBackendApi()).post(BASE_ROUTE, step);
  },
  async delete(stepId) {
    return (await getBackendApi()).delete(BASE_ROUTE + "/" + stepId);
  },
};
