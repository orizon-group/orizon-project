import getBackendApi from "./backend-api";

const BASE_ROUTE = "goals";

export default {
  async getAll(offset = 0) {
    return (await getBackendApi()).get(BASE_ROUTE, { params: { offset: offset } });
  },
  async create(goal, steps) {
    return (await getBackendApi()).post(BASE_ROUTE, {
      request_goal: goal,
      request_steps: steps,
    });
  },
  async get(goalId) {
    return (await getBackendApi()).get(BASE_ROUTE + "/" + goalId);
  },
  async update(goal, steps, goalId) {
    return (await getBackendApi()).put(BASE_ROUTE + "/" + goalId, {
      request_goal: goal,
      request_steps: steps,
    });
  },
  async delete(goalId) {
    return (await getBackendApi()).delete(BASE_ROUTE + "/" + goalId);
  },
  async search(goalTitle) {
    return (await getBackendApi()).get(BASE_ROUTE + "/search/" + goalTitle);
  },
};
