import { toastController } from "@ionic/vue";

export default {
    async openErrorToast(message) {
        const toast = await toastController.create({
            color: "danger",
            message: message,
            duration: 4000,
        });
        return toast.present();
    },
};
