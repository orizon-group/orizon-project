import getBackendApi from "./backend-api";

const BASE_ROUTE = "posts";

export default {
  async getFeed(offset = 0) {
    return (await getBackendApi()).get(BASE_ROUTE, { params: { offset: offset } });
  },
  async create(post) {
    return (await getBackendApi()).post(BASE_ROUTE, post);
  },
  async delete(postId) {
    return (await getBackendApi()).delete(BASE_ROUTE + "/" + postId);
  },
  async like(postId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + postId + "/like");
  },
  async unlike(postId) {
    return (await getBackendApi()).post(BASE_ROUTE + "/" + postId + "/unlike");
  },
  async getSupporters(postId, offset = 0) {
    return (await getBackendApi()).get(BASE_ROUTE + "/" + postId + "/supporters",
    { params: { offset: offset } });
  },
};
