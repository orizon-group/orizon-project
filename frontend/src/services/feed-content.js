export default {
  feedContent: null,
  setFeedContent(ionContent) {
    this.feedContent = ionContent;
  },
  getFeedContent() {
    return this.feedContent;
  },
};
