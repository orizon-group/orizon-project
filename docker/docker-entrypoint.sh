#!/bin/sh

set -e

. /venv/bin/activate

echo "---- Upgrading DB ----"
aerich upgrade
echo "---- DB upgraded ----"

exec gunicorn -c src/gunicorn_config_prod.py src.main:app --forwarded-allow-ips="*"