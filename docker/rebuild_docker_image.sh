#!/bin/sh

PROJECT_NAME="orizon-project"
CONTAINER_NAME="$PROJECT_NAME-container"
IMAGE_NAME="$PROJECT_NAME-image"

echo "Building new image ..."
sudo docker build --cache-from $IMAGE_NAME:latest -t $IMAGE_NAME:latest -f Dockerfile ..

echo "Starting new container ..."
sudo docker run --rm --name $CONTAINER_NAME -e "PORT=5000" --env-file ../backend/.env -p 5000:5000 $IMAGE_NAME:latest

