from fastapi import APIRouter, Depends
from loguru import logger
from src.api_v1.endpoints.user import user_service
from src.api_v1.http_constantes import (
    HTTP_NOT_FOUND,
    HTTP_NOT_FOUND_AND_INTERNAL_SERVER_ERROR,
    HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
from src.firebase import get_current_user
from src.models import User, Visibility
from src.schemas import ResponseUser, ResponseCurrentUser, ResponseUserSlim

router = APIRouter()


@router.get(
    path="/me",
    response_model=ResponseCurrentUser,
    responses=HTTP_NOT_FOUND,
)
async def get_current_user_info(current_user: User = Depends(get_current_user)):
    logger.info("[Current User ID: {}] Get current user info", current_user.id)
    response_current_user = await ResponseCurrentUser.from_tortoise_orm(current_user)
    logger.info("User({})", response_current_user)
    return response_current_user


@router.get(
    path="/{user_id}/followers",
    response_model=list[ResponseUserSlim],
    responses=HTTP_NOT_FOUND,
)
async def get_user_followers(
    user_id: int,
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get followers of user with id {} and offset {}",
        current_user.id,
        user_id,
        offset,
    )
    response_users = await user_service.get_user_followers(
        user_id=user_id,
        offset=offset,
        current_user=current_user,
    )
    logger.info("Followers({})", response_users)
    return response_users


@router.get(
    path="/{user_id}/following",
    response_model=list[ResponseUserSlim],
    responses=HTTP_NOT_FOUND,
)
async def get_user_following(
    user_id: int,
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get following of user with id {} and offset {}",
        current_user.id,
        user_id,
        offset,
    )
    response_users = await user_service.get_user_following(
        user_id=user_id,
        offset=offset,
        current_user=current_user,
    )
    logger.info("Following({})", response_users)
    return response_users


@router.put(
    path="/me/public",
    responses=HTTP_NOT_FOUND_AND_INTERNAL_SERVER_ERROR,
)
async def make_current_user_public(current_user: User = Depends(get_current_user)):
    logger.info("[Current User ID: {}] Make public", current_user.id)
    await user_service.change_visibility(current_user, Visibility.PUBLIC)
    return "Successfully switched to public visibility"


@router.put(
    path="/me/private",
    responses=HTTP_NOT_FOUND_AND_INTERNAL_SERVER_ERROR,
)
async def make_current_user_private(current_user: User = Depends(get_current_user)):
    logger.info("[Current User ID: {}] Make private", current_user.id)
    await user_service.change_visibility(current_user, Visibility.PRIVATE)
    return "Successfully switched to private visibility"


@router.get(
    path="/{user_id}",
    response_model=ResponseUser,
    responses=HTTP_NOT_FOUND,
)
async def get_user(
    user_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get user with id {}",
        current_user.id,
        user_id,
    )
    response_user = await user_service.get_by_id(
        user_id=user_id,
        current_user=current_user,
    )
    logger.info("User({})", response_user)
    return response_user


@router.get(
    path="/search/{fullname}",
    response_model=list[ResponseUserSlim],
)
async def search_user(
    fullname: str,
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] search users with name {} and offset {}",
        current_user.id,
        fullname,
        offset,
    )
    response_users = await user_service.search(
        fullname=fullname,
        offset=offset,
        current_user=current_user,
    )
    logger.info("Users({})", response_users)
    return response_users


@router.delete(
    path="/me",
    responses=HTTP_NOT_FOUND_AND_INTERNAL_SERVER_ERROR,
)
async def delete_user(current_user: User = Depends(get_current_user)):
    logger.info("[Current User ID: {}] Delete account", current_user.id)
    await user_service.delete(current_user)
    return "Successfully deleted account"


@router.post(
    path="/{user_id}/follow",
    responses=HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
async def follow_user(
    user_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Follow user with id {}",
        current_user.id,
        user_id,
    )
    return await user_service.follow(
        user_id=user_id,
        current_user=current_user,
    )


@router.post(
    path="/{requester_id}/accept_follow",
    responses=HTTP_NOT_FOUND,
)
async def accept_follow_request(
    requester_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Accept follow request from user with id {}",
        current_user.id,
        requester_id,
    )
    await user_service.accept_follow_request(
        requester_id=requester_id,
        current_user=current_user,
    )
    return f"Successfully accepted follow request from user with id {requester_id}"


@router.post(
    path="/{requester_id}/deny_follow",
    responses=HTTP_NOT_FOUND,
)
async def deny_follow_request(
    requester_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Deny follow request from user with id {}",
        current_user.id,
        requester_id,
    )
    await user_service.deny_follow_request(
        requester_id=requester_id,
        current_user=current_user,
    )
    return f"Successfully denied follow request from user with id {requester_id}"


@router.post(
    path="/{user_id}/cancel_follow",
    responses=HTTP_NOT_FOUND,
)
async def cancel_follow_request(
    user_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Cancel follow request to user with id {}",
        current_user.id,
        user_id,
    )
    await user_service.cancel_follow_request(
        user_id=user_id,
        current_user=current_user,
    )
    return f"Successfully cancelled follow request to user with id {user_id}"


@router.post(
    path="/{user_id}/unfollow",
    responses=HTTP_NOT_FOUND_AND_UNPROCESSABLE_ENTITY,
)
async def unfollow_user(
    user_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Unfollow user with id {}",
        current_user.id,
        user_id,
    )
    await user_service.unfollow(
        user_id=user_id,
        current_user=current_user,
    )
    return f"Successfully unfollowed user with id {user_id}"


@router.put(
    path="/{user_id}/remove_follow",
    responses=HTTP_NOT_FOUND,
)
async def remove_follower(
    user_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Remove follower with id {}",
        current_user.id,
        user_id,
    )
    await user_service.remove_follower(
        user_id=user_id,
        current_user=current_user,
    )
    return f"Successfully removed follower with id {user_id}"
