from fastapi import APIRouter, Depends
from loguru import logger
from src.api_v1.endpoints.comment import comment_service
from src.api_v1.http_constantes import HTTP_NOT_FOUND
from src.firebase import get_current_user
from src.models import User
from src.schemas import (
    RequestCreateComment,
    ResponseComment,
)

router = APIRouter()


@router.get(
    path="/post/{post_id}",
    response_model=list[ResponseComment],
)
async def get_all_comments(
    post_id: int,
    offset: int = 0,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Get comments of post with id {} and offset {}",
        current_user.id,
        post_id,
        offset,
    )
    response_comments = await comment_service.get_all(
        post_id=post_id,
        offset=offset,
        current_user=current_user,
    )
    logger.info("Comments({})", response_comments)
    return response_comments


@router.post(
    path="/",
    response_model=ResponseComment,
    responses=HTTP_NOT_FOUND,
)
async def create_comment(
    request_comment: RequestCreateComment,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Create Comment({})",
        current_user.id,
        request_comment,
    )
    return await comment_service.create(
        request_comment=request_comment,
        current_user=current_user,
    )


@router.delete(
    path="/{comment_id}",
    responses=HTTP_NOT_FOUND,
)
async def delete_comment(
    comment_id: int,
    current_user: User = Depends(get_current_user),
):
    logger.info(
        "[Current User ID: {}] Delete comment with id {}",
        current_user.id,
        comment_id,
    )
    await comment_service.delete(
        comment_id=comment_id,
        current_user=current_user,
    )
    return f"Successfully deleted comment with id {comment_id}"
