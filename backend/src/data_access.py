from fastapi.exceptions import HTTPException
from src.models import Goal, Post, User, Reply, Comment, Visibility
from starlette.status import HTTP_403_FORBIDDEN

HTTP_403_FORBIDDEN_EXCEPTION = HTTPException(
    status_code=HTTP_403_FORBIDDEN,
    detail="Access forbidden",
)


async def assert_current_user_is_not_following_user(
    user: User,
    current_user: User,
):
    if await _is_following_user(
        user=user,
        current_user=current_user,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_current_user_is_following_user(
    user: User,
    current_user: User,
):
    if not await _is_following_user(
        user=user,
        current_user=current_user,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_current_user_is_followed_by_user(
    user: User,
    current_user: User,
):
    if not await _is_following_user(
        user=current_user,
        current_user=user,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_requester_is_in_user_follow_requests(
    requester: User,
    user: User,
):
    await user.fetch_related("received_follow_requests")
    if requester not in user.received_follow_requests:
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_current_user_is_reply_owner(
    reply_id: int,
    current_user_id: int,
):
    if not await _is_reply_owner(
        reply_id=reply_id,
        current_user_id=current_user_id,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_current_user_is_comment_owner(
    comment_id: int,
    current_user_id: int,
):
    if not await _is_comment_owner(
        comment_id=comment_id,
        current_user_id=current_user_id,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_current_user_is_goal_owner(
    goal_id: int,
    current_user_id: int,
):
    if not await _is_goal_owner(
        goal_id=goal_id,
        current_user_id=current_user_id,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_current_user_is_post_owner(
    post_id: int,
    current_user_id: int,
):
    if not await _is_post_owner(
        post_id=post_id,
        current_user_id=current_user_id,
    ):
        raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_goal_is_accessible(
    goal_id: int,
    current_user: User,
):
    if await _is_goal_owner(
        goal_id=goal_id,
        current_user_id=current_user.id,
    ):
        return

    if await _is_public_goal(goal_id):
        return

    if await _is_following_goal_owner(
        goal_id=goal_id,
        current_user=current_user,
    ):
        return

    raise HTTP_403_FORBIDDEN_EXCEPTION


async def assert_that_post_is_accessible(
    post_id: int,
    current_user: User,
):
    if await _is_post_owner(
        post_id=post_id,
        current_user_id=current_user.id,
    ):
        return

    if await _is_public_post(post_id):
        return

    if await _is_following_post_owner(
        post_id=post_id,
        current_user=current_user,
    ):
        return

    raise HTTP_403_FORBIDDEN_EXCEPTION


async def _is_following_user(
    user: User,
    current_user: User,
) -> bool:
    await user.fetch_related("followers")
    return current_user in user.followers


async def _is_public_goal(goal_id: int) -> bool:
    goal = await Goal.get(id=goal_id).prefetch_related("user")
    return goal.user.visibility == Visibility.PUBLIC


async def _is_public_post(post_id: int) -> bool:
    post = await Post.get(id=post_id).prefetch_related("goal__user")
    return post.goal.user.visibility == Visibility.PUBLIC


async def _is_reply_owner(reply_id: int, current_user_id: int) -> bool:
    return await Reply.exists(id=reply_id, writer_id=current_user_id)


async def _is_comment_owner(comment_id: int, current_user_id: int) -> bool:
    return await Comment.exists(id=comment_id, writer_id=current_user_id)


async def _is_goal_owner(goal_id: int, current_user_id: int) -> bool:
    return await Goal.exists(id=goal_id, user_id=current_user_id)


async def _is_post_owner(post_id: int, current_user_id: int) -> bool:
    return await Post.exists(id=post_id, goal__user_id=current_user_id)


async def _is_following_goal_owner(goal_id: int, current_user: User) -> bool:
    following_ids = [following.id async for following in current_user.following]
    return await Goal.exists(id=goal_id, user_id__in=following_ids)


async def _is_following_post_owner(post_id: int, current_user: User) -> bool:
    following_ids = [following.id async for following in current_user.following]
    return await Post.exists(id=post_id, goal__user_id__in=following_ids)
