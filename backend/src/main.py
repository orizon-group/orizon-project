import uvicorn
from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
from src.api_v1.api_router import api_router
from src.api_v1.http_constantes import HTTP_UNAUTHORIZED
from src.app_config import (
    ALLOW_CREDENTIALS,
    ALLOWED_HEADERS,
    ALLOWED_METHODS,
    ALLOWED_ORIGINS,
    API_PATH_PREFIX,
    PROJECT_NAME,
    TORTOISE_ORM_CONFIG,
    ADD_EXCEPTION_HANDLERS,
    GENERATE_SCHEMAS,
    OPENAPI_URL,
    SWAGGER_UI_OAUTH2_REDIRECT_URL,
    DOCS_URL,
    REDOC_URL,
)
from src.firebase import get_current_user
from tortoise.contrib.fastapi import register_tortoise

app = FastAPI(
    title=PROJECT_NAME,
    dependencies=[Depends(get_current_user)],
    responses=HTTP_UNAUTHORIZED,
    openapi_url=OPENAPI_URL,
    swagger_ui_oauth2_redirect_url=SWAGGER_UI_OAUTH2_REDIRECT_URL,
    docs_url=DOCS_URL,
    redoc_url=REDOC_URL,
)

app.add_middleware(
    CORSMiddleware,
    allow_credentials=ALLOW_CREDENTIALS,
    allow_origins=ALLOWED_ORIGINS,
    allow_methods=ALLOWED_METHODS,
    allow_headers=ALLOWED_HEADERS,
)

app.include_router(
    api_router,
    prefix=API_PATH_PREFIX,
)

register_tortoise(
    app,
    config=TORTOISE_ORM_CONFIG,
    generate_schemas=GENERATE_SCHEMAS,
    add_exception_handlers=ADD_EXCEPTION_HANDLERS,
)


def main():
    uvicorn.run(
        "src.main:app",
        host="0.0.0.0",
        port=5000,
        reload=True,
        debug=True,
        workers=1,
    )


if __name__ == "__main__":
    main()
