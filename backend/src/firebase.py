import json

import firebase_admin
from fastapi import Security, Depends
from fastapi.exceptions import HTTPException
from fastapi.security.http import HTTPAuthorizationCredentials, HTTPBearer
from firebase_admin import credentials, auth
from loguru import logger
from src.data_validation.email import assert_that_email_is_valid
from src.data_validation.enum import assert_that_value_is_in_enum
from src.data_validation.uid import assert_that_uid_is_valid
from src.models import User
from src.models import Visibility
from src.schemas import RequestUser
from starlette.status import HTTP_401_UNAUTHORIZED
from src.app_config import FIREBASE_CREDENTIALS_JSON

if FIREBASE_CREDENTIALS_JSON:
    FIREBASE_CREDENTIALS = credentials.Certificate(
        json.loads(FIREBASE_CREDENTIALS_JSON)
    )
    firebase_admin.initialize_app(FIREBASE_CREDENTIALS)


async def get_credentials(
    authorization_credentials: HTTPAuthorizationCredentials = Security(HTTPBearer()),
) -> str:
    return authorization_credentials.credentials


async def get_current_user(credentials: str = Depends(get_credentials)) -> User:
    try:
        request_user = _verify_credentials_and_get_user_data(credentials)
        user = await _create_or_update_user_by_uid(request_user)
    except Exception as e:
        logger.error(e)
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    else:
        return user


async def _create_or_update_user_by_uid(request_user: RequestUser) -> User:
    uid = request_user.uid
    assert_that_value_is_in_enum(
        value=request_user.visibility,
        enum=Visibility,
    )
    assert_that_email_is_valid(request_user.email)
    assert_that_uid_is_valid(uid)
    if not await User.exists(uid=uid):
        return await User.create(**request_user.dict(exclude_unset=True))
    await User.filter(uid=uid).update(
        **request_user.dict(exclude_unset=True, exclude={"visibility"})
    )
    return await User.get(uid=uid)


def delete_user(uid: str):
    auth.delete_user(uid=uid)


def _verify_credentials_and_get_user_data(credentials: str) -> RequestUser:
    decoded_token = _verify_credentials(credentials)
    user_data = _get_user_data(uid=decoded_token["uid"])
    return _parse_user_data(user_data)


def _verify_credentials(credentials: str) -> dict:
    return auth.verify_id_token(credentials)


def _get_user_data(uid: str) -> dict:
    return auth.get_user(uid=uid).__dict__["_data"]


def _parse_user_data(user_data: dict) -> RequestUser:
    return RequestUser(
        uid=user_data["localId"],
        email=user_data["email"],
        profile_picture_url=user_data["photoUrl"],
        fullname=user_data["displayName"],
        visibility=Visibility.PUBLIC,
    )
