from typing import Optional
from pydantic.main import BaseModel
from src.models import Comment, GoalStatus, Post, Reply, Step, User, Goal, Visibility
from enum import Enum
from tortoise.contrib.pydantic import pydantic_model_creator
from datetime import datetime

RequestUser = pydantic_model_creator(
    User,
    name="RequestUser",
    exclude_readonly=True,
)


class ResponseUserSlim(BaseModel):
    id: int
    uid: str
    fullname: str
    profile_picture_url: str

    @classmethod
    def from_tortoise_orm(cls, user: User) -> "ResponseUserSlim":
        return cls(
            id=user.id,
            uid=user.uid,
            fullname=user.fullname,
            profile_picture_url=user.profile_picture_url,
        )


class ResponseUserBase(BaseModel):
    id: int
    fullname: str
    profile_picture_url: str
    visibility: Visibility
    registration_date: datetime
    number_of_followers: int
    number_of_following: int

    @classmethod
    async def from_tortoise_orm(cls, user: User) -> "ResponseUserBase":
        await user.fetch_related("followers", "following")
        return cls(
            id=user.id,
            fullname=user.fullname,
            profile_picture_url=user.profile_picture_url,
            visibility=user.visibility,
            registration_date=user.registration_date,
            number_of_followers=len(user.followers),
            number_of_following=len(user.following),
        )


class ResponseGoalSlim(BaseModel):
    id: int
    title: str
    status: GoalStatus

    @classmethod
    def from_tortoise_orm(cls, goal: Goal) -> "ResponseGoalSlim":
        return cls(
            id=goal.id,
            title=goal.title,
            status=goal.status,
        )


class ResponseUser(ResponseUserBase):
    is_requested_to_follow: bool
    is_followed: bool
    goals: list[ResponseGoalSlim]

    @classmethod
    async def from_tortoise_orm(
        cls,
        user: User,
        current_user: User,
    ) -> "ResponseUser":
        base_user = await ResponseUserBase.from_tortoise_orm(user)
        await user.fetch_related("goals", "received_follow_requests", "followers")
        return cls(
            **base_user.dict(),
            is_requested_to_follow=current_user in user.received_follow_requests,
            is_followed=current_user in user.followers,
            goals=[ResponseGoalSlim.from_tortoise_orm(goal) for goal in user.goals],
        )


class ResponseCurrentUser(ResponseUserBase):
    number_of_goals: int
    received_follow_requests: list[ResponseUserSlim]

    @classmethod
    async def from_tortoise_orm(cls, user: User) -> "ResponseCurrentUser":
        base_user = await ResponseUserBase.from_tortoise_orm(user)
        await user.fetch_related("goals", "received_follow_requests")
        return cls(
            **base_user.dict(),
            number_of_goals=len(user.goals),
            received_follow_requests=[
                ResponseUserSlim.from_tortoise_orm(user)
                for user in user.received_follow_requests
            ],
        )


# Reply
RequestCreateReply = pydantic_model_creator(
    Reply,
    name="RequestCreateReply",
    exclude_readonly=True,
    exclude=("writer_id",),
)


class ResponseReply(BaseModel):
    id: int
    date: datetime
    content: str
    writer: ResponseUserSlim

    @classmethod
    async def from_tortoise_orm(cls, reply: Reply) -> "ResponseReply":
        await reply.fetch_related("writer")
        return cls(
            id=reply.id,
            date=reply.date,
            content=reply.content,
            writer=ResponseUserSlim.from_tortoise_orm(reply.writer),
        )


RequestCreateComment = pydantic_model_creator(
    Comment,
    name="RequestCreateComment",
    exclude_readonly=True,
    exclude=("writer_id",),
)


class ResponseComment(BaseModel):
    id: int
    date: datetime
    content: str
    writer: ResponseUserSlim
    replies: list[ResponseReply]

    @classmethod
    async def from_tortoise_orm(cls, comment: Comment) -> "ResponseComment":
        await comment.fetch_related("writer")
        return cls(
            id=comment.id,
            date=comment.date,
            content=comment.content,
            writer=ResponseUserSlim.from_tortoise_orm(comment.writer),
            replies=[
                await ResponseReply.from_tortoise_orm(reply)
                async for reply in comment.replies
            ],
        )


RequestPost = pydantic_model_creator(
    Post,
    name="RequestPost",
    exclude_readonly=True,
)


class ResponseGoalInner(BaseModel):
    id: int
    title: str
    user: ResponseUserSlim
    status: GoalStatus

    @classmethod
    async def from_tortoise_orm(cls, goal: Goal) -> "ResponseGoalInner":
        await goal.fetch_related("user")
        return cls(
            id=goal.id,
            title=goal.title,
            user=ResponseUserSlim.from_tortoise_orm(goal.user),
            status=goal.status,
        )


class ResponsePostInner(BaseModel):
    id: int
    date: datetime
    description: str
    number_of_comments_and_replies: int
    number_of_likes: int
    is_liked: bool

    @classmethod
    async def from_tortoise_orm(
        cls,
        post: Post,
        current_user: User,
    ) -> "ResponsePostInner":
        await post.fetch_related("likers", "comments", "comments__replies")
        return cls(
            id=post.id,
            date=post.date,
            description=post.description,
            number_of_comments_and_replies=(
                len(post.comments)
                + sum(len(comment.replies) for comment in post.comments)
            ),
            number_of_likes=len(post.likers),
            is_liked=current_user in post.likers,
        )


class ResponsePost(ResponsePostInner):
    goal: ResponseGoalInner

    @classmethod
    async def from_tortoise_orm(
        cls,
        post: Post,
        current_user: User,
    ) -> "ResponsePost":
        post_inner = await ResponsePostInner.from_tortoise_orm(
            post=post,
            current_user=current_user,
        )
        await post.fetch_related("goal")
        return cls(
            **post_inner.dict(),
            goal=await ResponseGoalInner.from_tortoise_orm(post.goal),
        )


# Step
RequestStepWithGoalId = pydantic_model_creator(
    Step,
    name="RequestStepWithGoalId",
    exclude_readonly=True,
)
RequestStepWithoutGoalId = pydantic_model_creator(
    Step,
    name="RequestStepWithoutGoalId",
    exclude_readonly=True,
    exclude=("goal_id",),
)


class ResponseStep(BaseModel):
    id: int
    description: str
    completion_date: Optional[datetime]

    @classmethod
    def from_tortoise_orm(cls, step: Step) -> "ResponseStep":
        return cls(
            id=step.id,
            description=step.description,
            completion_date=step.completion_date,
        )


RequestGoal = pydantic_model_creator(
    Goal,
    name="RequestGoal",
    exclude_readonly=True,
    exclude=("user_id",),
)


class ResponseGoal(BaseModel):
    id: int
    title: str
    description: str
    user: ResponseUserSlim
    status: GoalStatus
    steps: list[ResponseStep]
    posts: list[ResponsePostInner]

    @classmethod
    async def from_tortoise_orm(
        cls,
        goal: Goal,
        current_user: User,
    ) -> "ResponseGoal":
        await goal.fetch_related("user", "steps", "posts")
        return cls(
            id=goal.id,
            title=goal.title,
            description=goal.description,
            user=ResponseUserSlim.from_tortoise_orm(goal.user),
            status=goal.status,
            steps=[ResponseStep.from_tortoise_orm(step) for step in goal.steps],
            posts=[
                await ResponsePostInner.from_tortoise_orm(
                    post,
                    current_user=current_user,
                )
                for post in goal.posts
            ],
        )


class FeedbackCategory(str, Enum):
    BUG = "BUG"
    SUGGESTION = "SUGGESTION"
    LOVE = "LOVE"
    OTHER = "OTHER"


class RequestFeedback(BaseModel):
    title: str
    message: str
    category: FeedbackCategory
