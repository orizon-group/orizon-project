from enum import Enum

from src.app_config import MODELS_FILE_PATH
from src.data_validation.FieldNotEmptyValidator import FieldNotEmptyValidator
from tortoise import Tortoise, fields
from tortoise.models import Model

USER_MODEL = "models.User"


# Goal
class GoalStatus(str, Enum):
    TODO = "TODO"
    ONGOING = "ONGOING"
    DONE = "DONE"


class Goal(Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(
        max_length=70, validators=[FieldNotEmptyValidator("title")]
    )
    description = fields.TextField()
    status = fields.CharEnumField(enum_type=GoalStatus)
    creation_date = fields.DatetimeField(auto_now_add=True)
    steps: fields.ReverseRelation["Step"]
    posts: fields.ReverseRelation["Post"]
    user: fields.ForeignKeyRelation["User"] = fields.ForeignKeyField(
        USER_MODEL,
        related_name="goals",
        to_field="id",
    )


# Post
class Post(Model):
    id = fields.IntField(pk=True)
    description = fields.CharField(max_length=255)
    date = fields.DatetimeField(auto_now_add=True)
    likers: fields.ManyToManyRelation["User"] = fields.ManyToManyField(
        USER_MODEL,
        related_name="liked_posts",
        through="user_like_post",
    )
    comments: fields.ReverseRelation["Comment"]
    goal: fields.ForeignKeyRelation["Goal"] = fields.ForeignKeyField(
        "models.Goal",
        related_name="posts",
        to_field="id",
    )


# User
class Visibility(str, Enum):
    PUBLIC = "PUBLIC"
    PRIVATE = "PRIVATE"


class User(Model):
    id = fields.IntField(pk=True)
    uid = fields.CharField(max_length=200, unique=True)
    email = fields.CharField(max_length=320, unique=True)
    fullname = fields.CharField(max_length=70)
    profile_picture_url = fields.CharField(max_length=255)
    registration_date = fields.DatetimeField(auto_now_add=True)
    visibility = fields.CharEnumField(enum_type=Visibility)
    liked_posts: fields.ManyToManyRelation["Post"]
    goals: fields.ReverseRelation["Goal"]
    followers: fields.ManyToManyRelation["User"] = fields.ManyToManyField(
        USER_MODEL,
        related_name="following",
        through="user_follow_user",
        forward_key="follower_id",
    )
    following: fields.ManyToManyRelation["User"]
    comments: fields.ReverseRelation["Comment"]
    replies: fields.ReverseRelation["Reply"]
    received_follow_requests: fields.ManyToManyRelation[
        "User"
    ] = fields.ManyToManyField(
        USER_MODEL,
        related_name="sent_follow_requests",
        through="user_request_follow_user",
        forward_key="requester_id",
    )
    sent_follow_requests: fields.ManyToManyRelation["User"]


# Comment and Reply
class AbstractComment(Model):
    id = fields.IntField(pk=True)
    content = fields.TextField(validators=[FieldNotEmptyValidator("content")])
    date = fields.DatetimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Comment(AbstractComment):
    replies: fields.ReverseRelation["Reply"]
    post: fields.ForeignKeyRelation["Post"] = fields.ForeignKeyField(
        "models.Post",
        related_name="comments",
        to_field="id",
    )
    writer: fields.ForeignKeyRelation["User"] = fields.ForeignKeyField(
        USER_MODEL,
        related_name="comments",
        to_field="id",
    )


class Reply(AbstractComment):
    comment: fields.ForeignKeyRelation["Comment"] = fields.ForeignKeyField(
        "models.Comment",
        related_name="replies",
        to_field="id",
    )
    writer: fields.ForeignKeyRelation["User"] = fields.ForeignKeyField(
        "models.User",
        related_name="replies",
        to_field="id",
    )


# Step
class Step(Model):
    id = fields.IntField(pk=True)
    description = fields.CharField(
        max_length=70, validators=[FieldNotEmptyValidator("description")]
    )
    completion_date = fields.DatetimeField(null=True)
    goal: fields.ForeignKeyRelation["Goal"] = fields.ForeignKeyField(
        "models.Goal",
        related_name="steps",
        to_field="id",
    )


# Early models initialization (In order to have model relations in schemas generation)
Tortoise.init_models(MODELS_FILE_PATH, "models")
