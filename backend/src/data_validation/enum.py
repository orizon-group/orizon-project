from enum import Enum
from typing import Type

from fastapi.exceptions import HTTPException
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY


def assert_that_value_is_in_enum(value: str, enum: Type[Enum]):
    if value not in enum._value2member_map_:
        raise HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"{value} is not a possible value for its field",
        )
