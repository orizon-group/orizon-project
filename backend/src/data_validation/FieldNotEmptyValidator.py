from tortoise.validators import Validator
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from fastapi.exceptions import HTTPException


class FieldNotEmptyValidator(Validator):
    def __init__(self, field_name):
        self._field_name = field_name

    def __call__(self, value: str):
        if len(value) == 0:
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"{self._field_name} field can not be empty",
            )
