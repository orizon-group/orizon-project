import re

from fastapi.exceptions import HTTPException
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY

UID_PATTERN = re.compile(r"^[A-Za-z0-9_\-]+$")


def assert_that_uid_is_valid(uid: str):
    if not UID_PATTERN.match(uid):
        raise HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"'{uid}' is not a valid uid",
        )
