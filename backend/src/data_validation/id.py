from typing import Type

from fastapi.exceptions import HTTPException
from src.models import Comment, Goal, Post, Reply, User
from starlette.status import HTTP_404_NOT_FOUND, HTTP_422_UNPROCESSABLE_ENTITY
from tortoise.models import Model


def assert_user_ids_are_different(first_user_id: int, second_user_id: int):
    if first_user_id == second_user_id:
        raise HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY,
            detail="You can't follow nor unfollow yourself",
        )


async def assert_that_object_exists(
    model_name: str,
    model: Type[Model],
    object_id: int,
) -> None:
    is_object_in_db = await model.exists(id=object_id)
    if not is_object_in_db:
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail=f"{model_name} with id {object_id} does not exist",
        )


async def assert_that_comment_exists(comment_id: int):
    await assert_that_object_exists(
        model_name="Comment",
        model=Comment,
        object_id=comment_id,
    )


async def assert_that_reply_exists(reply_id: int):
    await assert_that_object_exists(
        model_name="Reply",
        model=Reply,
        object_id=reply_id,
    )


async def assert_that_goal_exists(goal_id: int):
    await assert_that_object_exists(
        model_name="Goal",
        model=Goal,
        object_id=goal_id,
    )


async def assert_that_post_exists(post_id: int):
    await assert_that_object_exists(
        model_name="Post",
        model=Post,
        object_id=post_id,
    )


async def assert_that_user_exists(user_id: int):
    await assert_that_object_exists(
        model_name="User",
        model=User,
        object_id=user_id,
    )
