-- upgrade --
ALTER TABLE "user" ADD "fullname" VARCHAR(60) NOT NULL;
ALTER TABLE "user" DROP COLUMN "lastname";
ALTER TABLE "user" DROP COLUMN "firstname";
-- downgrade --
ALTER TABLE "user" ADD "lastname" VARCHAR(50) NOT NULL;
ALTER TABLE "user" ADD "firstname" VARCHAR(50) NOT NULL;
ALTER TABLE "user" DROP COLUMN "fullname";
