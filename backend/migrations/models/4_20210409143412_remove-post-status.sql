-- upgrade --
ALTER TABLE "post" DROP COLUMN "status";
-- downgrade --
ALTER TABLE "post" ADD "status" VARCHAR(6) NOT NULL;
