-- upgrade --
ALTER TABLE "goal" ADD "creation_date" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE "goal" DROP COLUMN "end";
ALTER TABLE "goal" DROP COLUMN "begin";
-- downgrade --
ALTER TABLE "goal" ADD "end" TIMESTAMPTZ;
ALTER TABLE "goal" ADD "begin" TIMESTAMPTZ;
ALTER TABLE "goal" DROP COLUMN "creation_date";
