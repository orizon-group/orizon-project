-- upgrade --
ALTER TABLE "goal" DROP COLUMN "is_personal";
-- downgrade --
ALTER TABLE "goal" ADD "is_personal" BOOL NOT NULL  DEFAULT False;
