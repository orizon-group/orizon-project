import pytest
from fastapi import HTTPException
from src import firebase
from src.models import User, Visibility
from src.schemas import RequestUser
from tests.conftest import assert_model_objects_are_equal

pytestmark = pytest.mark.asyncio


async def test_create_or_update_user_by_uid_create(
    db_setup,
    freeze_time,
):
    # given
    utc_now = freeze_time
    request_user = RequestUser(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
    )
    expected_user = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )

    # when
    user = await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert_model_objects_are_equal(user, expected_user)


async def test_create_or_update_user_by_uid_createWithInvalidStatus(db_setup):
    # given
    request_user = RequestUser(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility="WRONG",
    )
    expected_exception_detail = "WRONG is not a possible value for its field"

    # when
    with pytest.raises(HTTPException) as excinfo:
        await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert excinfo.value.status_code == 422
    assert excinfo.value.detail == expected_exception_detail


async def test_create_or_update_user_by_uid_createWithInvalidEmail(db_setup):
    # given
    request_user = RequestUser(
        uid="CurrentUserUidUpdated",
        email="invalid email",
        fullname="current_user_firstname_updated current_user_lastname_updated",
        profile_picture_url="current_user_profile_picture_url_updated",
        visibility=Visibility.PUBLIC,
    )
    expected_exception_detail = "'invalid email' is not a valid email"

    # when
    with pytest.raises(HTTPException) as excinfo:
        await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert excinfo.value.status_code == 422
    assert excinfo.value.detail == expected_exception_detail


async def test_create_or_update_user_by_uid_createWithInvalidUid(db_setup):
    # given
    request_user = RequestUser(
        uid="invalid uid",
        email="current.user.updated@gmail.com",
        fullname="current_user_firstname_updated current_user_lastname_updated",
        profile_picture_url="current_user_profile_picture_url_updated",
        visibility=Visibility.PUBLIC,
    )
    expected_exception_detail = "'invalid uid' is not a valid uid"

    # when
    with pytest.raises(HTTPException) as excinfo:
        await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert excinfo.value.status_code == 422
    assert excinfo.value.detail == expected_exception_detail


async def test_create_or_update_user_by_uid_update(
    db_setup,
    freeze_time,
    prepare_current_user_data,
):
    # given
    utc_now = freeze_time
    request_user = RequestUser(
        uid="CurrentUserUid",
        email="current.user.updated@gmail.com",
        fullname="current_user_firstname_updated current_user_lastname_updated",
        profile_picture_url="current_user_profile_picture_url_updated",
        visibility=Visibility.PRIVATE,
    )
    expected_user = User(
        uid="CurrentUserUid",
        email="current.user.updated@gmail.com",
        fullname="current_user_firstname_updated current_user_lastname_updated",
        profile_picture_url="current_user_profile_picture_url_updated",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )

    # when
    user = await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert_model_objects_are_equal(user, expected_user)


async def test_create_or_update_user_by_uid_updateWithInvalidStatus(
    db_setup,
    prepare_current_user_data,
):
    # given
    request_user = RequestUser(
        uid="CurrentUserUid",
        email="current.user.updated@gmail.com",
        fullname="current_user_firstname_updated current_user_lastname_updated",
        profile_picture_url="current_user_profile_picture_url_updated",
        visibility="WRONG",
    )
    expected_exception_detail = "WRONG is not a possible value for its field"

    # when
    with pytest.raises(HTTPException) as excinfo:
        await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert excinfo.value.status_code == 422
    assert excinfo.value.detail == expected_exception_detail


async def test_create_or_update_user_by_uid_updateWithInvalidEmail(
    db_setup,
    prepare_current_user_data,
):
    # given
    request_user = RequestUser(
        uid="CurrentUserUid",
        email="invalid email",
        fullname="current_user_firstname_updated current_user_lastname_updated",
        profile_picture_url="current_user_profile_picture_url_updated",
        visibility=Visibility.PUBLIC,
    )
    expected_exception_detail = "'invalid email' is not a valid email"

    # when
    with pytest.raises(HTTPException) as excinfo:
        await firebase._create_or_update_user_by_uid(request_user)

    # then
    assert excinfo.value.status_code == 422
    assert excinfo.value.detail == expected_exception_detail


def test_parse_user_data():
    # given
    user_data = {
        "displayName": "Didier Super",
        "email": "didier-super@gmail.com",
        "localId": "JEqjjx7L5rRZq9FvCIgcQxbsUhr2",
        "photoUrl": "profile_picture_url",
    }
    expected_request_user = RequestUser(
        uid="JEqjjx7L5rRZq9FvCIgcQxbsUhr2",
        email="didier-super@gmail.com",
        profile_picture_url="profile_picture_url",
        fullname="Didier Super",
        visibility=Visibility.PUBLIC,
    )

    # when
    request_user = firebase._parse_user_data(user_data)

    # then
    assert request_user == expected_request_user
