import pytest
from httpx import AsyncClient
from src.models import Comment
from tests.conftest import assert_model_objects_are_equal

pytestmark = pytest.mark.asyncio

BASE_ROUTE = "/comments"


async def test_get_all_comments(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    current_user_post_id = 1

    # when
    response = await async_client.get(f"{BASE_ROUTE}/post/{current_user_post_id}")

    # then
    assert response.status_code == 200
    comments = response.json()
    assert len(comments) == 1

    utc_now = freeze_time
    expected_comment_schema = {
        "id": 1,
        "content": "current user comment content",
        "date": utc_now,
        "replies": [
            {
                "id": 1,
                "content": "current user reply content",
                "date": utc_now,
                "writer": {
                    "id": 1,
                    "uid": "CurrentUserUid",
                    "fullname": "current_user_firstname current_user_lastname",
                    "profile_picture_url": "current_user_profile_picture_url",
                },
            }
        ],
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert comments[0] == expected_comment_schema


async def test_get_all_comments_withPostIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_post_id = 99
    params = {"offset": 3}
    expected_response = {"detail": "Post with id 99 does not exist"}

    # when
    response = await async_client.get(
        f"{BASE_ROUTE}/post/{unexisting_post_id}",
        params=params,
    )

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_get_all_comments_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_post_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.get(f"{BASE_ROUTE}/post/{other_user_post_id}")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_create_comment_ownPost(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    current_user_post_id = 1
    utc_now = freeze_time
    request_body = {
        "content": "this is my comment",
        "post_id": current_user_post_id,
    }
    expected_object = Comment(
        content="this is my comment",
        date=utc_now,
        post_id=current_user_post_id,
        writer_id=1,
    )

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    comment = response.json()
    expected_comment_schema = {
        "id": 3,
        "content": "this is my comment",
        "date": utc_now,
        "replies": [],
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert comment == expected_comment_schema
    assert_model_objects_are_equal(await Comment.get(id=comment["id"]), expected_object)


async def test_create_comment_followingPost(
    async_client: AsyncClient,
    freeze_time,
    current_user_follow_other_user,
):
    # given
    other_user_post_id = 2
    utc_now = freeze_time
    request_body = {
        "content": "this is my comment",
        "post_id": other_user_post_id,
    }
    expected_object = Comment(
        content="this is my comment",
        date=utc_now,
        post_id=other_user_post_id,
        writer_id=1,
    )

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    comment = response.json()
    expected_comment_schema = {
        "id": 3,
        "content": "this is my comment",
        "date": utc_now,
        "replies": [],
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert comment == expected_comment_schema
    assert_model_objects_are_equal(await Comment.get(id=comment["id"]), expected_object)


async def test_create_comment_withOtherUserIsPublic(
    async_client: AsyncClient,
    freeze_time,
    make_other_user_public,
):
    # given
    other_user_post_id = 2
    utc_now = freeze_time
    request_body = {
        "content": "this is my comment",
        "post_id": other_user_post_id,
    }
    expected_object = Comment(
        content="this is my comment",
        date=utc_now,
        post_id=other_user_post_id,
        writer_id=1,
    )

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 200
    comment = response.json()
    expected_comment_schema = {
        "id": 3,
        "content": "this is my comment",
        "date": utc_now,
        "replies": [],
        "writer": {
            "id": 1,
            "uid": "CurrentUserUid",
            "fullname": "current_user_firstname current_user_lastname",
            "profile_picture_url": "current_user_profile_picture_url",
        },
    }
    assert comment == expected_comment_schema
    assert_model_objects_are_equal(await Comment.get(id=comment["id"]), expected_object)


async def test_create_comment_withPostIdDoesNotExist(async_client: AsyncClient):
    # given
    request_body = {
        "content": "this is my comment",
        "post_id": 99,
    }
    expected_response = {"detail": "Post with id 99 does not exist"}

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_create_comment_withEmptyContent(async_client: AsyncClient):
    # given
    request_body = {
        "content": "",
        "post_id": 1,
    }
    expected_response = {"detail": "content field can not be empty"}

    # when
    response = await async_client.post(
        BASE_ROUTE,
        json=request_body,
    )

    # then
    assert response.status_code == 422
    assert response.json() == expected_response


async def test_delete_comment(async_client: AsyncClient):
    # given
    comment_id = 1
    expected_response = "Successfully deleted comment with id 1"

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{comment_id}")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response
    assert not await Comment.exists(id=comment_id)


async def test_delete_comment_withAccessForbidden(async_client: AsyncClient):
    # given
    other_user_comment_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{other_user_comment_id}")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response
    assert await Comment.exists(id=other_user_comment_id)


async def test_delete_comment_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_comment_id = 99
    expected_response = {"detail": "Comment with id 99 does not exist"}

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/{unexisting_comment_id}")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response
