import pytest
from httpx import AsyncClient
from src.models import User, Visibility
from tests.conftest import assert_model_objects_are_equal

pytestmark = pytest.mark.asyncio

BASE_ROUTE = "/users"


async def test_get_user(
    async_client: AsyncClient,
    freeze_time,
    current_user_follow_other_user,
    current_user_request_follow_other_user,
):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}")

    # then
    assert response.status_code == 200
    user = response.json()
    utc_now = freeze_time
    expected_current_user_schema = {
        "id": 2,
        "is_followed": True,
        "is_requested_to_follow": True,
        "fullname": "other_user_firstname other_user_lastname",
        "registration_date": utc_now,
        "profile_picture_url": "other_user_profile_picture_url",
        "visibility": "PRIVATE",
        "number_of_followers": 1,
        "number_of_following": 0,
        "goals": [
            {
                "id": 2,
                "status": "TODO",
                "title": "other user goal title",
            }
        ],
    }
    assert user == expected_current_user_schema


async def test_get_user_withIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_user_id = 99
    expected_response = {"detail": "User with id 99 does not exist"}

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{unexisting_user_id}")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_get_current_user_info(
    async_client: AsyncClient,
    freeze_time,
    other_user_request_follow_current_user,
):
    # when
    response = await async_client.get(f"{BASE_ROUTE}/me")

    # then
    assert response.status_code == 200
    user = response.json()
    utc_now = freeze_time
    expected_current_user_schema = {
        "id": 1,
        "fullname": "current_user_firstname current_user_lastname",
        "registration_date": utc_now,
        "profile_picture_url": "current_user_profile_picture_url",
        "visibility": "PUBLIC",
        "number_of_goals": 1,
        "number_of_followers": 0,
        "number_of_following": 0,
        "received_follow_requests": [
            {
                "id": 2,
                "uid": "OtherUserUid",
                "fullname": "other_user_firstname other_user_lastname",
                "profile_picture_url": "other_user_profile_picture_url",
            }
        ],
    }
    assert user == expected_current_user_schema


async def test_get_user_followers_withOwnFollowers(
    async_client: AsyncClient,
    other_user_follow_current_user,
):
    # given
    current_user_id = 1

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{current_user_id}/followers")

    # then
    assert response.status_code == 200
    followers = response.json()
    assert len(followers) == 1
    expected_other_user_schema = {
        "id": 2,
        "uid": "OtherUserUid",
        "fullname": "other_user_firstname other_user_lastname",
        "profile_picture_url": "other_user_profile_picture_url",
    }
    assert followers[0] == expected_other_user_schema


async def test_get_user_followers_withFollowedUserFollowers(
    async_client: AsyncClient,
    current_user_follow_other_user,
):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}/followers")

    # then
    assert response.status_code == 200
    followers = response.json()
    assert len(followers) == 1
    expected_current_user_schema = {
        "id": 1,
        "uid": "CurrentUserUid",
        "fullname": "current_user_firstname current_user_lastname",
        "profile_picture_url": "current_user_profile_picture_url",
    }
    assert followers[0] == expected_current_user_schema


async def test_get_user_followers_withPublicUserFollowers(
    async_client: AsyncClient,
    make_other_user_public,
):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}/followers")

    # then
    assert response.status_code == 200
    followers = response.json()
    assert len(followers) == 0


async def test_get_user_followers_withAccessforbidden(async_client: AsyncClient):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}/followers")

    # then
    assert response.status_code == 403
    assert response.json() == {"detail": "Access forbidden"}


async def test_get_user_following_withOwnFollowing(
    async_client: AsyncClient,
    current_user_follow_other_user,
):
    # given
    current_user_id = 1

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{current_user_id}/following")

    # then
    assert response.status_code == 200
    following = response.json()
    assert len(following) == 1
    expected_other_user_schema = {
        "id": 2,
        "uid": "OtherUserUid",
        "fullname": "other_user_firstname other_user_lastname",
        "profile_picture_url": "other_user_profile_picture_url",
    }
    assert following[0] == expected_other_user_schema


async def test_get_user_following_withFollowedUserFollowing(
    async_client: AsyncClient,
    other_user_follow_current_user,
    current_user_follow_other_user,
):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}/following")

    # then
    assert response.status_code == 200
    following = response.json()
    assert len(following) == 1
    expected_current_user_schema = {
        "id": 1,
        "uid": "CurrentUserUid",
        "fullname": "current_user_firstname current_user_lastname",
        "profile_picture_url": "current_user_profile_picture_url",
    }
    assert following[0] == expected_current_user_schema


async def test_get_user_following_withPublicUserFollowing(
    async_client: AsyncClient,
    other_user_follow_current_user,
    make_other_user_public,
):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}/following")

    # then
    assert response.status_code == 200
    following = response.json()
    assert len(following) == 1
    expected_current_user_schema = {
        "id": 1,
        "uid": "CurrentUserUid",
        "fullname": "current_user_firstname current_user_lastname",
        "profile_picture_url": "current_user_profile_picture_url",
    }
    assert following[0] == expected_current_user_schema


async def test_get_user_following_withAccessforbidden(async_client: AsyncClient):
    # given
    other_user_id = 2

    # when
    response = await async_client.get(f"{BASE_ROUTE}/{other_user_id}/following")

    # then
    assert response.status_code == 403
    assert response.json() == {"detail": "Access forbidden"}


async def test_make_current_user_private(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    utc_now = freeze_time
    expected_user = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PRIVATE,
        registration_date=utc_now,
    )
    expected_response = "Successfully switched to private visibility"

    # when
    response = await async_client.put(f"{BASE_ROUTE}/me/private")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    current_user = await User.get(uid="CurrentUserUid")
    assert_model_objects_are_equal(current_user, expected_user)


async def test_make_current_user_public(
    async_client: AsyncClient,
    freeze_time,
    make_current_user_private,
):
    # given
    utc_now = freeze_time
    expected_user = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )
    expected_response = "Successfully switched to public visibility"

    # when
    response = await async_client.put(f"{BASE_ROUTE}/me/public")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    current_user = await User.get(uid="CurrentUserUid")
    assert_model_objects_are_equal(current_user, expected_user)


async def test_make_current_user_public_withPendingFollowRequest(
    async_client: AsyncClient,
    freeze_time,
    other_user_request_follow_current_user,
):
    # given
    requester_id = 2
    utc_now = freeze_time
    expected_user_object = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )
    expected_follower_object = User(
        uid="OtherUserUid",
        email="other.user@gmail.com",
        fullname="other_user_firstname other_user_lastname",
        profile_picture_url="other_user_profile_picture_url",
        visibility=Visibility.PRIVATE,
        registration_date=utc_now,
    )
    expected_response = "Successfully switched to public visibility"

    # when
    response = await async_client.put(f"{BASE_ROUTE}/me/public")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    current_user = await User.get(uid="CurrentUserUid").prefetch_related(
        "followers",
        "received_follow_requests",
    )
    assert len(current_user.received_follow_requests) == 0
    assert len(current_user.followers) == 1
    assert_model_objects_are_equal(current_user.followers[0], expected_follower_object)

    requester = await User.get(uid="OtherUserUid").prefetch_related(
        "following",
        "sent_follow_requests",
    )
    assert len(requester.sent_follow_requests) == 0
    assert len(requester.following) == 1
    assert_model_objects_are_equal(requester.following[0], expected_user_object)


async def test_delete_user(
    async_client: AsyncClient,
    freeze_time,
    mocker,
):
    # given
    mocker.patch(
        "src.api_v1.endpoints.user.user_service.firebase.delete_user",
        return_value=None,
    )

    # when
    response = await async_client.delete(f"{BASE_ROUTE}/me")

    # then
    assert response.status_code == 200
    assert response.json() == "Successfully deleted account"
    users = await User.all()
    assert len(users) == 1
    utc_now = freeze_time
    expected_object = User(
        uid="OtherUserUid",
        email="other.user@gmail.com",
        fullname="other_user_firstname other_user_lastname",
        profile_picture_url="other_user_profile_picture_url",
        visibility=Visibility.PRIVATE,
        registration_date=utc_now,
    )
    assert_model_objects_are_equal(users[0], expected_object)


async def test_follow_user_withUserIsPrivate(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    other_user_id = 2
    utc_now = freeze_time
    expected_requester_object = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )
    expected_user_object = User(
        uid="OtherUserUid",
        email="other.user@gmail.com",
        fullname="other_user_firstname other_user_lastname",
        profile_picture_url="other_user_profile_picture_url",
        visibility=Visibility.PRIVATE,
        registration_date=utc_now,
    )
    expected_response = "Successfully requested to follow user with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/follow")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    other_user = await User.get(id=other_user_id).prefetch_related(
        "followers",
        "received_follow_requests",
    )
    assert len(other_user.followers) == 0
    assert len(other_user.received_follow_requests) == 1
    assert_model_objects_are_equal(
        other_user.received_follow_requests[0], expected_requester_object
    )

    follower = await User.get(uid="CurrentUserUid").prefetch_related(
        "following",
        "sent_follow_requests",
    )
    assert len(follower.following) == 0
    assert len(follower.sent_follow_requests) == 1
    assert_model_objects_are_equal(
        follower.sent_follow_requests[0], expected_user_object
    )


async def test_follow_user_withUserIsPublic(
    async_client: AsyncClient,
    freeze_time,
    make_other_user_public,
):
    # given
    other_user_id = 2
    utc_now = freeze_time
    expected_follower_object = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )
    expected_user_object = User(
        uid="OtherUserUid",
        email="other.user@gmail.com",
        fullname="other_user_firstname other_user_lastname",
        profile_picture_url="other_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )
    expected_response = "Successfully followed user with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/follow")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    other_user = await User.get(id=other_user_id).prefetch_related(
        "followers",
        "received_follow_requests",
    )
    assert len(other_user.received_follow_requests) == 0
    assert len(other_user.followers) == 1
    assert_model_objects_are_equal(other_user.followers[0], expected_follower_object)

    follower = await User.get(uid="CurrentUserUid").prefetch_related(
        "following",
        "sent_follow_requests",
    )
    assert len(follower.sent_follow_requests) == 0
    assert len(follower.following) == 1
    assert_model_objects_are_equal(follower.following[0], expected_user_object)


async def test_follow_user_withUserIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_user_id = 99
    expected_response = {"detail": "User with id 99 does not exist"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{unexisting_user_id}/follow")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_follow_user_withSameIds(async_client: AsyncClient):
    # given
    current_user_id = 1
    expected_response = {"detail": "You can't follow nor unfollow yourself"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{current_user_id}/follow")

    # then
    assert response.status_code == 422
    assert response.json() == expected_response


async def test_follow_user_withAlreadyFollowing(
    async_client: AsyncClient,
    current_user_follow_other_user,
):
    # given
    other_user_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/follow")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_accept_follow_request(
    async_client: AsyncClient,
    freeze_time,
    other_user_request_follow_current_user,
):
    # given
    requester_id = 2
    utc_now = freeze_time
    expected_user_object = User(
        uid="CurrentUserUid",
        email="current.user@gmail.com",
        fullname="current_user_firstname current_user_lastname",
        profile_picture_url="current_user_profile_picture_url",
        visibility=Visibility.PUBLIC,
        registration_date=utc_now,
    )
    expected_follower_object = User(
        uid="OtherUserUid",
        email="other.user@gmail.com",
        fullname="other_user_firstname other_user_lastname",
        profile_picture_url="other_user_profile_picture_url",
        visibility=Visibility.PRIVATE,
        registration_date=utc_now,
    )
    expected_response = "Successfully accepted follow request from user with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{requester_id}/accept_follow")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    current_user = await User.get(uid="CurrentUserUid").prefetch_related(
        "followers",
        "received_follow_requests",
    )
    assert len(current_user.received_follow_requests) == 0
    assert len(current_user.followers) == 1
    assert_model_objects_are_equal(current_user.followers[0], expected_follower_object)

    requester = await User.get(id=requester_id).prefetch_related(
        "following",
        "sent_follow_requests",
    )
    assert len(requester.sent_follow_requests) == 0
    assert len(requester.following) == 1
    assert_model_objects_are_equal(requester.following[0], expected_user_object)


async def test_accept_follow_request_withRequesterNotInFollowRequests(
    async_client: AsyncClient,
):
    # given
    requester_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{requester_id}/accept_follow")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_deny_follow_request(
    async_client: AsyncClient,
    other_user_request_follow_current_user,
):
    # given
    requester_id = 2

    expected_response = "Successfully denied follow request from user with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{requester_id}/deny_follow")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    current_user = await User.get(uid="CurrentUserUid").prefetch_related(
        "followers",
        "received_follow_requests",
    )
    assert len(current_user.received_follow_requests) == 0
    assert len(current_user.followers) == 0

    requester = await User.get(id=requester_id).prefetch_related(
        "following",
        "sent_follow_requests",
    )
    assert len(requester.sent_follow_requests) == 0
    assert len(requester.following) == 0


async def test_deny_follow_request_withRequesterNotInFollowRequests(
    async_client: AsyncClient,
):
    # given
    requester_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{requester_id}/deny_follow")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_cancel_follow_request(
    async_client: AsyncClient,
    current_user_request_follow_other_user,
):
    # given
    other_user_id = 2
    expected_response = "Successfully cancelled follow request to user with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/cancel_follow")

    # then
    assert response.json() == expected_response
    assert response.status_code == 200

    current_user = await User.get(uid="CurrentUserUid").prefetch_related(
        "followers",
        "received_follow_requests",
    )
    assert len(current_user.received_follow_requests) == 0
    assert len(current_user.followers) == 0

    other_user = await User.get(id=other_user_id).prefetch_related(
        "following",
        "sent_follow_requests",
    )
    assert len(other_user.sent_follow_requests) == 0
    assert len(other_user.following) == 0


async def test_cancel_follow_request_withRequesterNotInFollowRequests(
    async_client: AsyncClient,
):
    # given
    other_user_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/cancel_follow")

    # then
    assert response.status_code == 403
    assert response.json() == expected_response


async def test_unfollow_user(
    async_client: AsyncClient,
    current_user_follow_other_user,
):
    # given
    other_user_id = 2
    expected_response = "Successfully unfollowed user with id 2"

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/unfollow")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    other_user = await User.get(id=other_user_id).prefetch_related("followers")
    assert len(other_user.followers) == 0

    unfollower = await User.get(uid="CurrentUserUid").prefetch_related("following")
    assert len(unfollower.following) == 0


async def test_unfollow_user_withUserIdDoesNotExist(async_client: AsyncClient):
    # given
    unexisting_user_id = 99
    expected_response = {"detail": "User with id 99 does not exist"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{unexisting_user_id}/unfollow")

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_unfollow_user_withSameIds(async_client: AsyncClient):
    # given
    current_user_id = 1
    expected_response = {"detail": "You can't follow nor unfollow yourself"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{current_user_id}/unfollow")

    # then
    assert response.status_code == 422
    assert response.json() == expected_response


async def test_unfollow_user_withNotFollowing(async_client: AsyncClient):
    # given
    other_user_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.post(f"{BASE_ROUTE}/{other_user_id}/unfollow")

    # then
    assert response.json() == expected_response
    assert response.status_code == 403


async def test_search_user_withFullName(async_client: AsyncClient):
    # given
    full_name = "other_user_firstname other_user_lastname"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{full_name}")

    # then
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 1
    expected_other_user_schema = {
        "id": 2,
        "uid": "OtherUserUid",
        "fullname": "other_user_firstname other_user_lastname",
        "profile_picture_url": "other_user_profile_picture_url",
    }
    assert users[0] == expected_other_user_schema


async def test_search_user_withFirstName(async_client: AsyncClient):
    # given
    first_name = "other_user_firstname"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{first_name}")

    # then
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 1
    expected_other_user_schema = {
        "id": 2,
        "uid": "OtherUserUid",
        "fullname": "other_user_firstname other_user_lastname",
        "profile_picture_url": "other_user_profile_picture_url",
    }
    assert users[0] == expected_other_user_schema


async def test_search_user_withLastName(async_client: AsyncClient):
    # given
    last_name = "other_user_lastname"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{last_name}")

    # then
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 1
    expected_other_user_schema = {
        "id": 2,
        "uid": "OtherUserUid",
        "fullname": "other_user_firstname other_user_lastname",
        "profile_picture_url": "other_user_profile_picture_url",
    }
    assert users[0] == expected_other_user_schema


async def test_search_user_withsearchCurrentUser(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    current_fullname = "current_user_firstname"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{current_fullname}")

    # then
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 0


async def test_search_user_withoutMatchingUser(
    async_client: AsyncClient,
    freeze_time,
):
    # given
    inexisting_fullname = "inexisting_fullname"

    # when
    response = await async_client.get(f"{BASE_ROUTE}/search/{inexisting_fullname}")

    # then
    assert response.status_code == 200
    users = response.json()
    assert len(users) == 0


async def test_remove_follower(
    async_client: AsyncClient,
    other_user_follow_current_user,
):
    # given
    current_user_id = 1
    other_user_id = 2
    expected_response = "Successfully removed follower with id 2"

    # when
    response = await async_client.put(f"{BASE_ROUTE}/{other_user_id}/remove_follow")

    # then
    assert response.status_code == 200
    assert response.json() == expected_response

    current_user = await User.get(id=current_user_id).prefetch_related("followers")
    assert len(current_user.followers) == 0

    other_user = await User.get(uid="OtherUserUid").prefetch_related("following")
    assert len(other_user.following) == 0


async def test_remove_follower_withOtherUserDoesNotExist(
    async_client: AsyncClient,
):
    # given
    unexisting_user_id = 99
    expected_response = {"detail": "User with id 99 does not exist"}

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{unexisting_user_id}/remove_follow"
    )

    # then
    assert response.status_code == 404
    assert response.json() == expected_response


async def test_remove_follower_withOtherDoesNotFollowCurrentUser(
    async_client: AsyncClient,
):
    # given
    unfollowing_user_id = 2
    expected_response = {"detail": "Access forbidden"}

    # when
    response = await async_client.put(
        f"{BASE_ROUTE}/{unfollowing_user_id}/remove_follow"
    )

    # then
    assert response.status_code == 403
    assert response.json() == expected_response
